// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express     = require('express');        // call express
var exphbs      = require('express-handlebars');
var app         = express();                 // define our app using express
var bodyParser  = require('body-parser');
var md          = require('marked');
var path        = require('path');

md.setOptions({
  sanitize: false
});

app.use(express.static(path.join(__dirname, 'public')));

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure handlebars to use html extension
app.engine('.hbs', exphbs({
  extname: '.hbs',
  defaultLayout: 'main',
  helpers: {
    sfIcon: function(strIcon, strClass) {
      return '<i class="sfIcon-' + strIcon + ' ' + strClass + '" aria-hidden="true"></i>';
    }
  }
}));
app.set('view engine', '.hbs');

var port = process.env.PORT || 22589;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:22589/)
router.get(/^[^.]+$/, function(req, res) {
  var sql = require("mssql"),
    strURL = req.url,
    objConfig = {
        user: 'sOmEUsEr',
        password: 'SomePassword123',
        server: 'some.server.com', 
        database: 'StoreFeeder-Website',
        options: {
          encrypt: true 
        } 
    },
    arrIcons = [
      'team',
      'meeting',
      'checklist',
      'stack',
      'document',
      'webPage',
      'listCart',
      'homeList',
      'deliveryTruck',
      'pyramid',
      'jigsaw',
      'secureBox',
      'handshake'
    ];

  function getNavPromise(NavID, ParentID) {
    return new sql.Connection(objConfig).connect().then(function() {
      var request = new sql.Request();

      request.input('NavID', NavID);
      request.input('ParentID', ParentID);
      return request.query('SELECT [Pages].[ID], [Pages].[Name], [Pages].[URL], [Pages].[Title], [Pages].[Description], [Pages].[Icon] FROM Pages INNER JOIN [Navigation] ON [Pages].[ID]=[Navigation].[PageID] WHERE [Navigation].[MenuID] = @NavID AND [Navigation].[ParentID]=@ParentID ORDER BY [Navigation].[Position]');
    }).catch(function(err) {
      if (err) showError(err);
    });
  }

  function getNavigation(NavID, ParentID) {
    var data = [];
    var promises = [];

    promises[promises.length] = getNavPromise(NavID, ParentID);
    promises[promises.length - 1].then(function(recordset) {

      if (recordset && recordset.length > 0) {
        data = recordset;

        for (i = 0; i < data.length; i++) {
          promises[promises.length] = getNavPromise(NavID, data[i].ID);
          data[i].children = promises[promises.length - 1].then(function(recordset) {

          }).catch(function(err) {
            if (err) showError(err);
            data[i].children = [];
          });
        }
      }
    }).catch(function(err) {
      if (err) showError(err);
      return [];
    });

    console.warn(promises);

    Promise.all(promises).then(function() {
      return data;
    });
  }
  // console.log('---------');
  // console.log(getNavigation(1, 0));
  // console.log('---------');

  function showError(err) {
    console.log(err);
    res.render('error', {});
  }
  
  if (strURL === '/') {
    res.render('home', {});
  } else {
    sql.connect(objConfig).then(function() {
      var request = new sql.Request();

      request.input('strURL', strURL);
      request.query('SELECT TOP 1 title,[content],banner FROM Pages WHERE URL=@strURL AND title IS NOT NULL', function(err, recordset) {
        if (err) {
          showError(err)
        } else {
          if (recordset.length === 1) {
            var objPage = {
              page: {
                title: recordset[0].title,
                content: md(recordset[0].content || ''),
                image: recordset[0].banner
              }
            };
            res.render('page', Object.assign({}, objPage));
          } else {
            res.render('error', {});
          }
        }
      });
    }).catch(function(err) {
      if (err) showError(err);
    });
  }

  //res.json({ message: 'hooray! welcome to our api!' });
});

// POST methods
// ----------------------------------------------------
router.route('/contact-us').post(function(req, res) {
  console.info(req.body);

  res.json({ message: 'Post received!' });
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /
app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port, function () {
  console.log('Magic happens on port ' + port);
});
