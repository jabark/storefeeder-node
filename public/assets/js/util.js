var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

$(function() {
  $('.sfCompanySlider').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    center: true,
    autoplay: true,

    itemClass: 'sfCompanySlider-slide owl-item',
    centerClass: 'isCenter',
    activeClass: 'isActive',
    controlsClass: 'sfCompanySlider-controls',
    navContainerClass: 'sfCompanySlider-nav',
    navClass: ['sfCompanySlider-navLeft', 'sfCompanySlider-navRight'],

    autoplayTimeout: 3000,
    navText: ['<i class="fa fa-chevron-left sfCompanySlider-navLeft" aria-hidden="true"></i>', '<i class="fa fa-chevron-right sfCompanySlider-navRight" aria-hidden="true"></i>'],
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 3
      },
      1024: {
        items: 4
      },
      1280: {
        items: 5
      }
    }
  });

  // Pause the slideshow if the window is not focused.
  $(window).on('focus', function() {
    $('.sfSlideshow').cycle('resume');
  }).on('blur', function() {
    $('.sfSlideshow').cycle('pause');
  });

  // Show slideshow navigation if multiple slides
  if ($('.sfSlideshow-slide:not(.cycle-sentinel)').length > 1) {
    $('.sfSlideshow-prev, .sfSlideshow-next, .sfSlideshow-pager').show();
  }

  // Video full screen
  $('.sfVideoList').on('click', '.sfVideoList-videoPreview', function() {
    $(this).next().addClass('isFullScreen').find('.sfVideoList-video')[0].play();
  });

  $('.sfVideoList').on('click', '.sfVideoList-videoClose', function() {
    $(this).parent().removeClass('isFullScreen').find('.sfVideoList-video')[0].pause();
  });

  // Put the footer always at the bottom
  var resizeTimer;
  function placeFooter() {
    var elFooter = document.querySelector('.sfFooter');
    elFooter.parentNode.style.paddingBottom = elFooter.offsetHeight + 'px';
    if (!elFooter.classList.contains('sfFooter--forceBottom')) {
      elFooter.classList.add('sfFooter--forceBottom');
    }
  }
  $(window).on('resize', function() {
    resizeTimer && clearTimeout(resizeTimer);
    resizeTimer = setTimeout(placeFooter, 100); // Make it only fire when resizing stops
  });
  placeFooter();

  // Ajax form posts
  $('form.ajax').submit(function (event) {
    event.preventDefault(); // stop form from submitting normally
    var form = $(this), // get the form
      dataToSend = form.serialize(), // get the submitted form items
      url = form.attr('action'), // where we're submitting the data to
      target = form.attr('target'), // class of an element we're targeting. Created if doesn't exist;
      objTarget;

    if (target && target.length) {
      objTarget = $('.' + target);
      if (!objTarget.length) {
        objTarget = $('<div class="' + target + '"><img class="ajaxLoader" src="/assets/images/ajax-loader.gif" alt="Loading..." /></div></div>');
      }
    } else {
      objTarget = $('<div class="sfForm-msg"><img class="ajaxLoader" src="/assets/images/ajax-loader.gif" alt="Loading..." /></div></div>');
    }

    if (objTarget.length) {
      form.before(objTarget);
      form.hide();
      objTarget.find('.ajaxLoader').show();
    }

    $.post(url, dataToSend, function (data) {
      // optional function to deal with returned data
    }).done(function(data) {
      // what to do when it's completed
      objTarget.empty();
      $('.' + form.attr('data-success')).show();
    }).fail(function(jqXHR, textStatus, errorThrown) {
      $('.sfForm-formError').remove();
      var errorMsg = textStatus;
      if (jqXHR.status > 0) {
        jqXHR.status + ': ' + jqXHR.statusText;
      }
      if (jqXHR.responseJSON) {
        errorMsg = jqXHR.responseJSON.error_description
      }
      errorMsg = $('<p class="sfForm-formError">The was an error with your submission: "' + errorMsg + '". Please try again.</p>');
      $('.sfForm-showOnError').show();

      objTarget.remove();
      errorMsg.prependTo(form);
      form.show();
    });
  });

  // IE11 Fix for the videos displaying in flex layout
  if (isIE11) {
    $('.sfVideoList-videoPreviewImg').each(function() {
      var width = $(this).outerWidth();
      var percentage = (($(this).outerHeight() / width) * 100) + '%';

      $(this).css('width', '100%').parent().css({'height': '0px', 'padding-bottom': percentage});
      $(this).closest('.sfVideoList').css('height', 'auto');
    });
  }

  // Parallax
  $('.parallax-window').parallax();

  // Scroll firer
  var scrollTimer,
    pos = $(window).scrollTop();

  function fireScroll() {
    pos = $(window).scrollTop();
    headerSizer(pos);
  }

  $(window).scroll(function () {
      scrollTimer && clearTimeout(scrollTimer);
      scrollTimer = setTimeout(fireScroll, 100); // Make it only fire when you stop scrolling
  });

  // Size headerSizer
  function headerSizer(pos) {
    if (pos > 0) {
      $('.sfHeader').css({height: '50px', backgroundColor: 'rgba(42, 44, 53, 1)'});
      $('.sfHeader-logo').css({height: '50px', padding: '5px 0'});
      $('.sfHeader-navListItemSub').css({top: '47px'});
    } else {
      $('.sfHeader').css({height: '', backgroundColor: ''});
      $('.sfHeader-logo').css({height: '', padding: ''});
      $('.sfHeader-navListItemSub').css({top: ''});
    }
  }
});